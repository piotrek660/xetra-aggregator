"""
API module with basic methods
"""
import os
from fastapi import FastAPI
import uvicorn
from xetraagg import xcalc


app = FastAPI()

@app.get("/")
def read_root():
    """
    Dummy method
    """
    return {"Hello": "World"}


@app.get("/health")
def read_health():
    """
    Health check for the runnig service
    """
    return {
        "CHART": os.environ.get('HELM_CHART_NAME','n/d'),
        "VERSION": os.environ.get('HELM_CHART_VERSION','-1'),
    }


@app.get("/xetra/{isin}")
def isin_stats(isin: str, start_date: str, end_date: str):
    """
    Returns aggregated xetra statistics for a given ISIN
    """

    data = xcalc.XetraAgg(start_date, end_date, isin)
    return {"response": data.agg()}

def run():
    """
    Start App
    """
    uvicorn.run(app, host="0.0.0.0", port=8080)
