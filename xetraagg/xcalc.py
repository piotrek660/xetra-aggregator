"""
Xetra ISIN statistics
"""

import functools
import json
import logging
from datetime import datetime
import os
from io import StringIO
from itertools import chain
from time import time
import uuid
import boto3
import pandas as pd
import redis


logger = logging.getLogger()
LOGLEVEL = os.environ.get('LOGLEVEL', 'INFO').upper()
logger.setLevel(LOGLEVEL)
logging.basicConfig(format='%(asctime)s.%(msecs)03d | %(levelname)s | %(message)s', datefmt='%Y-%m-%d %I:%M:%S')


def time_function(func):
    """
    Function decorator used to measure running time.
    """
    def wrapper(*args, **kwargs):
        request_id = uuid.uuid4().hex
        start = time()

        obj = args[0]
        key = f"isin:{obj.isin}:{obj.from_date}:{obj.end_date}"

        try:
            if obj.cache.exists(key):
                logging.info(f"found {key} in cache")
                tmp = obj.cache.hgetall(name=key)
                output = {i.decode(): json.loads(tmp[i].decode()) for i in tmp.keys()}
            else:
                output = func( *args, **kwargs)
                obj.cache.hset(name=key, mapping={i: json.dumps(output[i]) for i in output})
            code = 200
        except Exception as error:
            output = [str(error)]
            code = 400
        end = time()

        return {
            "function" : func.__name__,
            "seconds" : {end-start},
            "request-id" : request_id,
            "key" : key,
            "status_code" :  code,
            "data" : output
        }

    return wrapper


class XetraAgg():
    """
    Interact with the deutsche börse buckets.
    Get statistics for a given ISIN.
    """

    def __init__(self,from_date=None, to_date=None, isin='DE0005772206'):
        self.bucket_name = 'deutsche-boerse-xetra-pds'
        self.s3_client = boto3.client('s3')
        self.isin = isin
        self.from_date = from_date
        self.end_date = to_date
        self.cache = redis.Redis(host=os.environ.get('XETRA_CACHE','127.0.0.1'), port=6379)


    def get_dates(self)->list:
        """
        Returns a list of months to query for
        Format "YYYY-MM-"
        """

        end_date=self.end_date if self.end_date else os.environ.get('END_DATE', datetime.now().strftime("%Y-%m-%d"))
        from_date=self.from_date if self.from_date else "2018-01-01"

        dates = pd.date_range(start=from_date, end=end_date).tolist()
        all_dates = list(set([date.isoformat()[:10] for date in dates]))
        all_dates.sort()
        return all_dates


    def get_keys(self)->list:
        """
        Returns a list of not empty (!= ETag e455...) keys to scan for data

        Keyword Arguments:
        month_prefix -- S3 Prefix for CSV's
        """

        dates = self.get_dates()
        all_keys = []
        for date in dates:
            response = self.s3_client.list_objects_v2(
                Bucket=self.bucket_name,
                MaxKeys=1000,
                Prefix=date
            )

            if response['ResponseMetadata']['HTTPStatusCode'] == 200:
                assert len(response['Contents']) <= 950, "hmm, to many objects)"
                keys = [item['Key'] for item in response['Contents'] if item['ETag'] != '"e455bd5dd2f87598f58a87038aa01838"']
                all_keys.append(keys)
            else:
                raise ValueError(f"http code{response['ResponseMetadata']['HTTPStatusCode']}")
        return list(chain.from_iterable(all_keys))


    def scan_key(self, data_key: str):
        """
        Returns a DataFrame for single S3 Object (for a given ISIN)

        It uses S3 Select to query the file with subset of columns that are needed,
        with the condition (where) for the ISIN.

        Keyword Arguments:
        data_key -- S3 Key to be scanned
        """

        sql_stmt = f"""SELECT  "Date", "Time", StartPrice, EndPrice, TradedVolume FROM s3object s WHERE ISIN='{self.isin}'"""
        req = self.s3_client.select_object_content(
             Bucket=self.bucket_name,
             Key=data_key,
             ExpressionType='SQL',
             Expression=sql_stmt,
             InputSerialization={'CSV': {'FileHeaderInfo': 'Use'}},
             OutputSerialization={'CSV': {}},
        )

        records = []
        # looping through the payload of the AWS EventStream and getting one or more Records and Stats
        for event in req['Payload']:
            if 'Records' in event:
                records.append(event['Records']['Payload'])

        if records:
            csv_file = StringIO(records[0].decode('utf-8'))
            data = pd.read_csv(csv_file, names=["Date", "Time" , "StartPrice", "EndPrice", "TradedVolume"])
            return data


    def collect_data(self):
        """
        Returns a CSV with the data for given ISIN and date range (form 2018-01-01 to now|END_DATE)
        Iterates over each month, day to collect all the data for the ISIN.

        After the scan the data is aggregated into daily measures defined in the specification.
        """

        frames = []
        data_keys = self.get_keys()
        for item in data_keys:
            logging.debug(f"scan {item}")
            data = self.scan_key(item)
            frames.append(data)
        logging.info("collected data")
        return frames


    @time_function
    def agg(self):
        """
        Calculates the statistics for an isin
        """

        logging.info(f"collecting data for ISIN={self.isin}")
        frames = self.collect_data()
        if any(item is not None for item in frames):
            logging.info("calculating metrics ...")
            df_all = pd.concat(frames)
            logging.debug("end concat")
            df_all.sort_values(['Date', 'Time'], ascending=[True, True])

            # calculate sums, first StartPrice, last EndPrice --> for a day
            df_trades = df_all[['TradedVolume','Date']].groupby(['Date']).agg(['sum'])
            df_sprice = df_all[['StartPrice','Date']].groupby(['Date']).nth([0])
            df_eprice = df_all[['EndPrice','Date']].groupby(['Date']).nth([-1])

            # calculate perc change
            df_eprice['prev_end_price'] = df_eprice.EndPrice.shift(1)
            df_eprice['perc_change'] = round(((df_eprice.EndPrice - df_eprice.prev_end_price)/df_eprice.prev_end_price)*100,2)

            # combine all calcs, format names and styles
            result = pd.concat([df_trades, df_sprice, df_eprice], axis=1)
            result = result.drop(columns=['prev_end_price'])
            result.columns = ['daily_traded_volume', 'opening_price','closing_price','percent_change_prev_closing']
            result = result[['opening_price','closing_price','daily_traded_volume', 'percent_change_prev_closing']]
            result['date'] = result.index
            data = json.loads(result.to_json(orient='records'))

            stats = {}
            for tmp in data:
                stats[tmp['date']] = {x: tmp[x] for x in tmp if x != 'date'}
            logging.info("calculations done!")

            return stats
