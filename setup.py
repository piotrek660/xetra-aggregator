"""
Setup.py for Xetraagg
"""

import os
from setuptools import setup

setup(name='xetraagg',
      version=os.environ.get('PYPI_VERSION','0.0.0'),
      description='Xetra isin aggregator with some text. Awsome stuff! Versuch 2 ...',
      install_requires=[
        'pytest==6.2.5',
        'boto3==1.19.7',
        'pandas==1.3.4',
        'redis==3.5.3',
        'fastapi==0.68.1',
        'uvicorn==0.15.0',
        'requests==2.25.1'
      ],
      extras_require={
        'interactive': ['jupyterlab'],
      },
      entry_points={
        'console_scripts': [
            'xdemo=xetraagg.shell_cmd:main',
            'xrunner=xetraagg.api:run',
        ]
      },
      python_requires='>=3.6',
      setup_requires=['pytest-runner'],
      tests_require=['pytest'],
      url='https://gitlab.com/piotrek660/xetra-aggregator',
      author='Piotr Kostuch',
      author_email='kostuch.piotr@gmail.com',
      license='MIT',
      packages=['xetraagg'],
      zip_safe=False)
